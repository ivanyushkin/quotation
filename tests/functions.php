<?php
function fixture($fixtureName)
{
    return json_decode(file_get_contents(__DIR__ . '/fixtures/' . $fixtureName . '.json'), true);
}