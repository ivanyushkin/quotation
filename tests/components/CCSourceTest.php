<?php

namespace ivanyushkin\Quotation\tests\components;


use ivanyushkin\Quotation\components\CCSource;
use ivanyushkin\Quotation\models\QuoteInterval;
use PHPUnit\Framework\TestCase;

/**
 * Class CCSourceTest
 * @package tests\components
 */
class CCSourceTest extends TestCase
{
    /**
     *
     */
    public function testGetQuotes(): void
    {
        $source = new class ([
            'from'   => 'USD',
            'to'     => 'BTC',
            'limit'  => 10,
            'apiKey' => NULL,
        ]) extends CCSource
        {
            protected function doRequest($action, $requestParams): array
            {
                return fixture('CCSource.response');
            }
        };

        $quotes = $source->getQuotes();

        $this->assertCount(10, $quotes);

        $this->assertEquals(
            new QuoteInterval([
                'time'  => 1567163520,
                'close' => 0.0001046,
                'open'  => 0.0001046,
                'high'  => 0.0001046,
                'low'   => 0.0001046,
            ])
            , $quotes[0]);
    }
}