<?php

namespace ivanyushkin\Quotation\components;


use GuzzleHttp\Client;
use ivanyushkin\Quotation\models\Quote;
use ivanyushkin\Quotation\models\QuoteInterval;
use ivanyushkin\Quotation\models\QuoteSimple;

/**
 * Class CCSource
 * @package components
 */
class CCSource extends Source
{

    /**
     *
     */
    private const ACTION2URL = [
        'historical' => '/data/histominute',
        'latest'     => '/data/price',
    ];
    /**
     * @var string
     */
    public $apiKey;
    /**
     * @var string
     */
    public $apiBaseUrl = 'https://min-api.cryptocompare.com';
    /**
     * @var string
     */
    public $name = 'CryptoCompare';
    /**
     * @var int
     */
    public $limit = 10;
    /**
     * @var
     */
    protected $httpClient;

    /**
     * @return array
     */
    public function getQuotes(): array
    {
        $requestParams = [
            'api_key' => $this->apiKey,
            'limit'   => $this->limit,
            'fsym'    => $this->from,
            'tsym'    => $this->to,
        ];
        $response = $this->doRequest('historical', $requestParams);

        $result = [];
        foreach ($response['Data'] as $quote) {
            $result[] = new QuoteInterval([
                'time'  => $quote['time'],
                'close' => $quote['close'],
                'open'  => $quote['open'],
                'high'  => $quote['high'],
                'low'   => $quote['low'],
            ]);
        }

        return $result;
    }

    /**
     * @param $action
     * @param $requestParams
     * @return array
     */
    protected function doRequest($action, $requestParams): array
    {
        $queryUrl = $this->getUrl($action) . '?' . http_build_query($requestParams);

        $response = $this->getClient()->get($queryUrl, $requestParams);

        if ($response->getStatusCode() !== 200) {
            \Yii::error("Ошибка общения с api: {$response->getBody()}", "source.{$this->name}.quotes");
            return null;
        }

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param $action
     * @return string
     */
    private function getUrl($action): string
    {
        return $this->apiBaseUrl . self::ACTION2URL[$action];
    }

    /**
     * @return Client
     */
    protected function getClient(): Client
    {
        return $this->httpClient ?? $this->httpClient = new Client();
    }

    /**
     * @return Quote
     */
    public function getLatestQuote(): Quote
    {
        $requestParams = [
            'api_key' => $this->apiKey,
            'fsym'    => $this->from,
            'tsym'    => $this->to,
        ];

        $response = $this->doRequest('latest', $requestParams);

        return new QuoteSimple([
            'time'  => time(),
            'price' => $response[0]
        ]);
    }
}