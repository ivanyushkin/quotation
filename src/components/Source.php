<?php

namespace ivanyushkin\Quotation\components;

use ivanyushkin\Quotation\models\Quote;
use yii\base\UnknownMethodException;

/**
 * Класс реализующий получение моделей {@see Quote} через api трейдерских сервисов.
 * @package components
 */
class Source extends \yii\base\Component
{
    /**
     * @var array Параметры отправляемого запроса.
     */
    public $name;

    /**
     * @var string Код валюты из которой конвертируем.
     */
    public $from;

    /**
     * @var string Код валюты в которую конвертируем.
     */
    public $to;

    /**
     * @return Quote[] Возвращает список цен.
     */
    public function getQuotes(): array
    {
        throw new UnknownMethodException('Метод не реализован.');
    }

    /**
     * @return Quote Возвращает последнюю возможную отметку цены.
     */
    public function getLatestQuote(): Quote
    {
        throw new UnknownMethodException('Метод не реализован.');
    }
}