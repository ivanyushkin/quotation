<?php

namespace ivanyushkin\Quotation\models;

/**
 * Запись цены с интервалом
 * @package models
 * @property float $close
 * @property float $open
 * @property float $high
 * @property float $low
 */
class QuoteInterval extends Quote
{
    /**
     * @var float Цена закрытия
     */
    public $close;
    /**
     * @var float Цена открытия
     */
    public $open;
    /**
     * @var float Макс. цена в периоде
     */
    public $high;
    /**
     * @var float Мин. цена в периоде
     */
    public $low;
}