<?php

namespace ivanyushkin\Quotation\models;

use yii\base\BaseObject;

/**
 * Модель реализующая хранение одной записи цен.
 * @package models
 */
abstract class Quote extends BaseObject
{
    /**
     * @var float $time Время записи
     */
    public $time;
}