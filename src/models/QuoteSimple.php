<?php

namespace ivanyushkin\Quotation\models;


/**
 * Class QuoteSimple
 * @package models
 * @property float $price
 */
class QuoteSimple extends Quote
{
    /**
     * @var float Цена
     */
    public $price;
}